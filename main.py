###
# Задача - Найти из множества кофеен близлежайшую к объекту person
# Кофейня это массив из трех чисел [x,y,d] где x,y -координаты на плоскости а d
# отображение ростаяния до person в попугаях.
#
# В переменной args записаны входные данные
###

###
# Задача со звездочкой
# Не использовать для решения сортировку
###
person = [0, 0]
buffer = [-1, -1, -1, -1, -1]


def get_third(x: list) -> int:
    return x[2]


def nearest_caffee(cafees: list) -> [[]]:
    caffees = sorted(cafees, key=lambda x: x[2])
    return [caffees[0]]


if __name__ == '__main__':
    args = [
        [1, 1, 1],
        [2, 2, 2],
        [3, 3, 3],
        [4, 4, 1],
        [4, 4, 3],
        [0, 0, 0],
    ]
    res = [
        [0, 0, 0]
    ]
    res_ = nearest_caffee(args)
    print(res_)
    assert len(res_) <= 5
    assert res_ == res
